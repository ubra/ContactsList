/**
 * Copyright 2017 ChenHao Dendi
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hgdendi.contactslist;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.github.promeg.pinyinhelper.Pinyin;
import com.github.promeg.tinypinyin.lexicons.android.cncity.CnCityDict;
import com.hgdendi.contactslist.common.FloatingBarItemDecorations;
import com.hgdendi.contactslist.common.IndexBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

public class ContactListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private LinkedHashMap<Integer, String> mHeaderList;
    private ArrayList<ShareContactsBean> mContactList;
    private ContactsListAdapter mAdapter;
    private PopupWindow mOperationInfoDialog;
    private View mLetterHintView;
    private IndexBar indexBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        // 添加中文城市词典
        Pinyin.init(Pinyin.newConfig().with(CnCityDict.getInstance(this)));
        fetchContactList();
        initView();
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                indexBar.setNavigators(true,mHeaderList);

            }
        });
    }

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.share_add_contact_recyclerview);
        mRecyclerView.setLayoutManager(mLayoutManager = new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(
                new FloatingBarItemDecorations(this, mHeaderList));
        initAdapter();
        mRecyclerView.setAdapter(mAdapter);
        IndexBar indexBar = (IndexBar) findViewById(R.id.share_add_contact_sidebar);
        indexBar.setNavigators(mHeaderList);
        indexBar = (IndexBar) findViewById(R.id.share_add_contact_sidebar);
        indexBar.setOnTouchingLetterChangedListener(new IndexBar.OnTouchingLetterChangeListener() {
            @Override
            public void onTouchingLetterChanged(int position) {
//                showLetterHintDialog(s);
                mLayoutManager.scrollToPositionWithOffset(position, 0);

        }

        @Override
        public void onTouchingStart (int position){
//                showLetterHintDialog(s);
        }

        @Override
        public void onTouchingEnd (int position){
//                hideLetterHintDialog();
        }
    });
}


    /**
     * related to {@link IndexBar#mOnTouchingLetterChangeListener}
     * show dialog in the center of this window
     *
     * @param s
     */
    private void showLetterHintDialog(String s) {
        if (mOperationInfoDialog == null) {
            mLetterHintView = getLayoutInflater().inflate(R.layout.dialog_letter_hint, null);
            mOperationInfoDialog = new PopupWindow(mLetterHintView, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, false);
            mOperationInfoDialog.setOutsideTouchable(true);
        }
        ((TextView) mLetterHintView.findViewById(R.id.dialog_letter_hint_textview)).setText(s);
        getWindow().getDecorView().post(new Runnable() {
            @Override
            public void run() {
                mOperationInfoDialog.showAtLocation(getWindow().getDecorView().findViewById(android.R.id.content), Gravity.CENTER, 0, 0);
            }
        });
    }


    private void hideLetterHintDialog() {
        mOperationInfoDialog.dismiss();
    }

    private void initAdapter() {
        mAdapter = new ContactsListAdapter(LayoutInflater.from(this),
                mContactList);
        mAdapter.setOnContactsBeanClickListener(new ContactsListAdapter.OnContactsBeanClickListener() {
            @Override
            public void onContactsBeanClicked(ShareContactsBean bean) {
                Snackbar.make(mRecyclerView, "lklk" , Snackbar.LENGTH_INDEFINITE).show();
////                snackbar.show();
////                new Snackbar();
//                snackbar.setAction(R.mipmap.ic_launcher, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//                final Snackbar.SnackbarLayout snackbarView = (Snackbar.SnackbarLayout) snackbar.getView();
//                final TextView snackbar_text = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
//                snackbar_text.setBackgroundResource(R.mipmap.ic_launcher);
//                snackbar_text.setHeight(20);
//                snackbar_text.setWidth(20);
//                snackbarView.setBackgroundColor(Color.parseColor("#ffffff"));
//                SnackbarAddView(snackbar,R.layout.dialog_letter_hint,Gravity.BOTTOM);
//                snackbar.show();
            }
        });
    }

    /**
     * 向Snackbar中添加view
     *
     * @param snackbar Snackbar
     * @param layoutId 需要添加的布局的id
     * @param index    新加布局在Snackbar中的位置
     */
    public static void SnackbarAddView(Snackbar snackbar, @LayoutRes int layoutId, int index) {
        View snackbarView = snackbar.getView();
        Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbarView;

        View addView = LayoutInflater.from(snackbarView.getContext()).inflate(layoutId, null);

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                .WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        p.gravity = Gravity.CENTER_VERTICAL;
        snackbarLayout.addView(addView, index, p);
    }



    protected void fetchContactList() {
        if (mHeaderList == null) {
            mHeaderList = new LinkedHashMap<>();
        }

        mContactList = new ArrayList<>();
        mContactList.add(new ShareContactsBean("收到啦A", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦B", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦AF", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "Z"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "B"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        mContactList.add(new ShareContactsBean("收到啦", "133", "A"));
        preOperation();


    }


    /**
     * calculate the TAG and add to {@link #mHeaderList}
     */
    private void preOperation() {
        mHeaderList.clear();
        if (mContactList.size() == 0) {
            return;
        }
        Collections.sort(mContactList, new Comparator<ShareContactsBean>() {
            @Override
            public int compare(ShareContactsBean o1, ShareContactsBean o2) {

                return o1.getmAbbreviation().compareTo(o2.getmAbbreviation());
            }
        });
        addHeaderToList(0, mContactList.get(0).getmAbbreviation());
        for (int i = 1; i < mContactList.size(); i++) {
            if (!mContactList.get(i - 1).getmAbbreviation().equalsIgnoreCase(mContactList.get(i).getmAbbreviation())) {
                addHeaderToList(i, mContactList.get(i).getmAbbreviation());
            }
        }

    }

    private void addHeaderToList(int index, String header) {
        mHeaderList.put(index, header);
    }

}
