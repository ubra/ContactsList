/**
 * Copyright 2017 ChenHao Dendi
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hgdendi.contactslist;

import android.util.Log;

import com.github.promeg.pinyinhelper.Pinyin;
import com.hgdendi.contactslist.common.Abbreviated;

class ShareContactsBean{
    private final String mName;
    private final String mPhone;
    private String mAbbreviation;
    private final String mInitial;

    ShareContactsBean(String name, String phone) {
        this.mName = name;
        this.mPhone = phone;
        if (Pinyin.isChinese(name.charAt(0))) {
            this.mAbbreviation = Pinyin.toPinyin(name, "");


        } else {
            this.mAbbreviation = name;
        }
        if (!mAbbreviation.matches("[A-Z]+")) {
            mAbbreviation = "#";
        }
        this.mInitial = mAbbreviation.substring(0, 1);
    }

    ShareContactsBean(String name, String phone, String initial) {
        this.mName = name;
        this.mPhone = phone;
        this.mAbbreviation = initial;
        if (!mAbbreviation.matches("[A-Z]+")) {
            mAbbreviation = "#";
        }
        this.mInitial = mAbbreviation;

    }

    ShareContactsBean(String name, String phone, String initial,boolean isjumpe) {
        this.mName = name;
        this.mPhone = phone;
        this.mAbbreviation = initial;
        if (!isjumpe||!mAbbreviation.matches("[A-Z]+")) {
            mAbbreviation = "#";
        }
        this.mInitial = mAbbreviation;

    }

//    @Override
//    public String getInitial() {
//        return mInitial;
//    }

    public String getName() {
        return mName;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getmAbbreviation() {
        return mAbbreviation;
    }

    public void setmAbbreviation(String mAbbreviation) {
        this.mAbbreviation = mAbbreviation;
    }

    //    @Override
//    public int compareTo(ShareContactsBean r) {
//        if (mAbbreviation.equals(r.mAbbreviation)) {
//            return 0;
//        }
//        boolean flag;
//        if ((flag = mAbbreviation.startsWith("#")) ^ r.mAbbreviation.startsWith("#")) {
//            return flag ? 1 : -1;
//        }
//        return getInitial().compareTo(r.getInitial());
//    }
}