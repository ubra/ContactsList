/**
 * Copyright 2017 ChenHao Dendi
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hgdendi.contactslist.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.hgdendi.contactslist.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class IndexBar extends View {
    private float mLetterSpacingExtra;

    private OnTouchingLetterChangeListener mOnTouchingLetterChangeListener;
    private List<String> mNavigators;
    private List<String> mNavigator;
    private LinkedHashMap<Integer, String> list;

    private int mFocusIndex;
    private Paint mPaint;
    private Paint mFocusPaint;
    private float mBaseLineHeight;
    private boolean isChange;

    public IndexBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initSetting(context, attrs);
    }

    public IndexBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSetting(context, attrs);
    }

    public IndexBar(Context context) {
        super(context);
    }

    /******************
     * external interfaces
     ******************/

    /**
     * set Listener
     *
     * @param onTouchingLetterChangeListener
     */
    public void setOnTouchingLetterChangedListener(
            OnTouchingLetterChangeListener onTouchingLetterChangeListener) {
        this.mOnTouchingLetterChangeListener = onTouchingLetterChangeListener;
    }

    /**
     * set the letters , will be displayed aside
     * and also related with the OnTouchingLetterChangeListener#onTouchingLetterChanged
     *
     * @param list
     */
    public void setNavigators(LinkedHashMap<Integer, String> list) {
        this.list = list;
        mNavigators = new ArrayList<>(list.values());
        requestLayout();
    }

    public void setNavigators(boolean isChange, LinkedHashMap<Integer, String> list) {
        this.isChange = isChange;
        this.list = list;
        if (isChange) {
            mNavigators = mNavigator;
            mNavigator = new ArrayList<>(list.values());
        } else {
            mNavigators = new ArrayList<>(list.values());
        }
        requestLayout();

    }


    /******************
     * common
     ******************/

    private void initSetting(Context context, AttributeSet attrs) {
        mNavigator = new ArrayList<>();
        mNavigators = new ArrayList<>(0);
        mNavigator.add("#");
        for (int i = 65; i < 91; i++) {
            mNavigator.add(""+((char)i));
        }

        mOnTouchingLetterChangeListener = getDummyListener();
        mFocusIndex = -1;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.IndexBar);
        float textSize = typedArray.getDimension(R.styleable.IndexBar_letterSize, 8);
        int letterColor = typedArray.getColor(R.styleable.IndexBar_letterColor,
                ContextCompat.getColor(getContext(), android.R.color.white));
        mLetterSpacingExtra = typedArray.getFloat(R.styleable.IndexBar_letterSpacingExtra, 1.4f);
        int focusLetterColor = typedArray.getColor(R.styleable.IndexBar_focusLetterColor,
                ContextCompat.getColor(getContext(), android.R.color.white));
        typedArray.recycle();

        mPaint = new Paint();
        mPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mPaint.setAntiAlias(true);
        mPaint.setColor(letterColor);
        mPaint.setTextSize(textSize);

        mFocusPaint = new Paint();
        mFocusPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mFocusPaint.setAntiAlias(true);
        mFocusPaint.setFakeBoldText(true);
        mFocusPaint.setTextSize(textSize);
        mFocusPaint.setColor(focusLetterColor);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int widthMeasureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(widthMeasureSpec);
        int specSize = MeasureSpec.getSize(widthMeasureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = getSuggestedMinWidth();
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int getSuggestedMinWidth() {
        String maxLengthTag = "";
        for (String tag : mNavigators) {
            if (maxLengthTag.length() < tag.length()) {
                maxLengthTag = tag;
            }
        }
        return (int) (mPaint.measureText(maxLengthTag) + 0.5);
    }

    private int measureHeight(int heightMeasureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(heightMeasureSpec);
        int specSize = MeasureSpec.getSize(heightMeasureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            Paint.FontMetrics fm = mPaint.getFontMetrics();
            float singleHeight = fm.bottom - fm.top;
            mBaseLineHeight = fm.bottom * mLetterSpacingExtra;
            result = (int) (mNavigators.size() * singleHeight * mLetterSpacingExtra);
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int height = getHeight();
        int width = getWidth();
        if (height == 0) {
            return;
        }
        int singleHeight = height / mNavigators.size();

        for (int i = 0; i < mNavigators.size(); i++) {
            float xPos = width / 2 - mPaint.measureText(mNavigators.get(i)) / 2;
            float yPos = singleHeight * (i + 1);
            if (i == mFocusIndex) {
                canvas.drawText(mNavigators.get(i), xPos, yPos - mBaseLineHeight, mFocusPaint);
            } else {
                canvas.drawText(mNavigators.get(i), xPos, yPos - mBaseLineHeight, mPaint);
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final float y = event.getY();
        final int formerFocusIndex = mFocusIndex;
        final OnTouchingLetterChangeListener listener = mOnTouchingLetterChangeListener;
        final int c = calculateOnClickItemNum(y);

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mFocusIndex = -1;
                invalidate();
//                if (isChange) {
//                    for (Integer position : list.keySet()) {
//                        if (list.get(position).equals(mNavigators.get(c))) {
//                            listener.onTouchingEnd(i);
//                            continue;
//                        }
//                    }
//                } else {
//                    for (Integer position : list.keySet()) {
//                        if (list.get(position).equals(mNavigators.get(c))) {
//                            listener.onTouchingEnd(i);
//                            continue;
//                        }
//                    }
//                }
                for (Integer position : list.keySet()) {
                    if (list.get(position).equals(mNavigators.get(c))) {
                        listener.onTouchingEnd(position);
                        continue;
                    }
                }
                break;
            case MotionEvent.ACTION_DOWN:
//                if (isChange) {
//                    for (int i = 0; i < mNavigators.size(); i++) {
//                        if (mNavigator.get(i).equals(mNavigators.get(c))) {
//                            listener.onTouchingStart(i);
//                            continue;
//                        }
//                    }
//                } else {
//                    listener.onTouchingStart(c);
//                }
                for (Integer position : list.keySet()) {
                    if (list.get(position).equals(mNavigators.get(c))) {
                        listener.onTouchingStart(position);
                        continue;
                    }
                }
            default:
                if (formerFocusIndex != c) {
                    if (c >= 0 && c < mNavigators.size()) {
//                        if (isChange) {
//                            for (int i = 0; i < mNavigator.size(); i++) {
//                                if (mNavigator.get(i).equals(mNavigators.get(c))) {
//                                    listener.onTouchingLetterChanged(i);
//                                    continue;
//                                }
//                            }
//
//                        } else {
//                            listener.onTouchingLetterChanged(c);
//                        }
                        for (Integer position : list.keySet()) {
                            if (list.get(position).equals(mNavigators.get(c))) {
                                listener.onTouchingLetterChanged(position);
                                continue;
                            }
                        }
                        mFocusIndex = c;
                        invalidate();
                    }
                }
                break;
        }
        return true;
    }

    /**
     * @param yPos
     * @return the corresponding position in list
     */
    private int calculateOnClickItemNum(float yPos) {
        int result;
        result = (int) (yPos / getHeight() * mNavigators.size());
        if (result >= mNavigators.size()) {
            result = mNavigators.size() - 1;
        } else if (result < 0) {
            result = 0;
        }
        return result;
    }

    public interface OnTouchingLetterChangeListener {
        void onTouchingLetterChanged(int position);

        void onTouchingStart(int position);

        void onTouchingEnd(int position);
    }

    private OnTouchingLetterChangeListener getDummyListener() {
        return new OnTouchingLetterChangeListener() {
            @Override
            public void onTouchingLetterChanged(int position) {

            }

            @Override
            public void onTouchingStart(int position) {

            }

            @Override
            public void onTouchingEnd(int position) {

            }
        };
    }

}
